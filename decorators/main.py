import time
from functools import wraps

"""
Time a function call in seconds
"""
def time(fn):
  from functools import wraps
  @wraps(fn)
  def wrap(*args, **kwargs):
    import time
    print(f"Running {fn.__name__}")
    start = time.perf_counter()
    print(f"Starting timing")
    ret = fn(*args, **kwargs)
    total = time.perf_counter() - start
    print(f"{fn.__name__} took {total:.6f} seconds \n")
    return ret
  return wrap



"""
Wraps a function and logs the output
Possibly pass in your logger name or logger
"""
def log_funcs(logger=None):
  def wrap(fn):
    @wraps(fn)
    def log_wrap(*args, **kwargs):
      import  logging
      if logger != None:
        our_logger = logger
      else:
        our_logger = logging.getLogger(__name__)
        our_logger.setLevel(logging.INFO)
        # Breaks global
        logging.basicConfig(format=f"{fn.__name__} return value: %(message)s")
      ret = fn(*args, **kwargs)
      our_logger.info("hi")
      return ret if ret else None
    return log_wrap
  return wrap


@time_dec
def main(word):
  return f"Hello {word}"


@log_funcs()
def main2(word):
  return f"Hello {word}"


if __name__ == "__main__":
  # print(main("one"))
  print(main2("two"))
