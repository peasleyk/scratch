"""
An abomination class to string together string operations on a string
AKA function composition fun
"""
from __future__ import annotations

class Apply:
    def __init__(self, a: str) -> None:
        self.a = a

    def __rshift__(self, f) -> Apply:
        self.a = f(self.a)
        return self

    def __repr__(self):
        return self.a

def upper(a: str) -> str:
    return a.upper()


def trim(a: str) -> str:
    return a.replace("\n", "").replace("\t", "")


def replace(a: str) -> str:
    return a.replace("Z", "L")


def lower(a: str) -> str:
    return a.lower()


def prepend(a: str) -> str:
    return f"pre: {a}"


test = "\tHez\nzo \tWo\trz\nd"
print(test)

res = (
        Apply(test)
            >> upper
            >> trim
            >> replace
            >> lower
            >> prepend
            >> upper
            >> trim
    )

print(res)