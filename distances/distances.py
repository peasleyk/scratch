
"""
Time a function call in seconds
"""
def time(fn):
  from functools import wraps
  @wraps(fn)
  def wrap(*args, **kwargs):
    import time
    print(f"Running {fn.__name__}")
    start = time.perf_counter()
    print(f"Starting timing")
    ret = fn(*args, **kwargs)
    total = time.perf_counter() - start
    print(f"{fn.__name__} took {total:.6f} seconds \n")
    return ret
  return wrap




# Levenshtein

"""
Wow this is slow as shit
"""

def recursive(a, b):
  if min(len(a), len(b)) == 0:
    return max(len(a),len(b))

  cost = 1
  if a[-1] == b[-1]:
    cost = 0

  return min (
    (recursive(a[:-1], b)+1),
    (recursive(a, b[:-1])+1),
    (recursive(a[:-1], b[:-1]) +cost))

"""
Wow this is fast as shit
"""
@time
def memoized(a,b):
  ss = [[0 for x in range(len(b)+1)] for y in range(len(a)+1)]

  '''
  0 0 0 0 0 0
  0 0 0 0 0 0
  0 0 0 0 0 0
  '''

  for x in range(1, len(a)+1):
    ss[x][0] = x
  for u in range(1, len(b)+1):
    ss[0][u] = u

  '''
  0 1 2 3 4 5
  1 0 0 0 0 0
  2 0 0 0 0 0
  '''


  for i in range(1, len(a)+1):
    for j in range(1, len(b)+1):
      cost = 1
      if a[i-1] == b[j-1]:
        cost = 0
      ss[i][j] = min(ss[i-1][j] + 1,
                     ss[i][j-1] + 1,
                     ss[i-1][j-1] + cost)

  return(ss[len(a)][len(b)])

@time
def DM_memoized(a,b):
  """
  Damerau–Levenshtein distance, OSAD
  Note the +1 when checking equality with a and b
  We need this as our matrix is actually one more row and column than our length,
  and we iterate one more than our actual string length
  """

  ss = [[0 for x in range(len(b)+1)] for y in range(len(a)+1)]

  '''
  0 0 0 0 0 0
  0 0 0 0 0 0
  0 0 0 0 0 0
  '''

  for x in range(1, len(a)+1):
    ss[x][0] = x
  for u in range(1, len(b)+1):
    ss[0][u] = u

  '''
  0 1 2 3 4 5
  1 0 0 0 0 0
  2 0 0 0 0 0
  '''

  for i in range(1, len(a)+1):
    for j in range(1, len(b)+1):
      cost = 1
      if a[i-1] == b[j-1]:
        cost = 0
      ss[i][j] = min(ss[i-1][j] + 1,
                     ss[i][j-1] + 1,
                     ss[i-1][j-1] + cost)

      if i > 1 and j > 1 and a[i-1] == b[j-2] and a[i-2] == b[j-1]:
        ss[i][j] = min(ss[i][j],
                       ss[i-2][j-2]+1,
                      )

  return(ss[len(a)][len(b)])

@time
def JS(a, b):
  matching = 0
  transpose = 0
  table = [[0 for x in range(len(b))] for y in range(len(a))]

  def distance(a, b):
    return((max(len(a),len(b))/2)-1)

  d = distance(a, b)

  for x in range(len(a)):
    for y in range(len(b)):
      if a[x] == b[y]:
        if abs(x-y) < d:
          if table[x][y] == 1:
            pass
          else:
            table[x][y] = 1
            matching+=1
        elif table[x][y] == 0:
          table[x][y] = 1
          transpose+=1

  # matching+=transpose
  transpose/=2

  return((1/3)*(
               (
                (matching/len(a))+
                (matching/len(b))+
                ((matching-transpose)/matching))
               )
              )
@time
def JS2(a, b):
  """
  Lovely wikipedia description , this is broke
   https://en.wikipedia.org/wiki/Jaro%E2%80%93Winkler_distance
  """
  if a == b:
    return 1

  matching = 0
  transpose = 0
  table = [[0 for x in range(len(b))] for y in range(len(a))]

  def distance(a, b):
    return(   (max(len(a),len(b))  /2 )  -1 )

  d = distance(a, b)

  for x in range(len(a)):
    for y in range(len(b)):
      if a[x] == b[y] and abs(x-y) < d and table[x][y] != 1:
        table[x][y] = 1
        if x == y:
          matching+=1
        if x != y:
            matching+=1
            transpose +=1


  transpose /= 2

  if matching == 0:
    return matching

  return((1/3)*(
               (
                (matching/len(a))+
                (matching/len(b))+
                ((matching-transpose)/matching))
               )
              )


a = "BAA"
b = "ABA"
print(f"Comparing {a} with {b} \n")
# print(f"Recursive Levenshtein distance:         {recursive(a, b)}")
print(f"Memoized Levenshtein distance:          {memoized(a, b)}")
print(f"Memoised  Damerau–Levenshtein distance: {DM_memoized(a,b)}")
print(f"Broken Jaro Distance:                   {JS(a,b):.3f}")
print(f"Less broken Jaro Distance2:                  {JS2(a,b):.3f}")